export default function createGame() {
  const state = {
    players: {},
    fruits: {},
    screen: {
      width: 10,
      height: 10
    }
  };

  const observers = [];

  function start() {
    const frequency = 5 * 1000;
    setInterval(addFruit, frequency);
  }

  function subscribe(observerFunction) {
    observers.push(observerFunction);
  }

  function notifyAll(command) {
    for (const observerFunction of observers) {
      observerFunction(command);
    }
  }

  function setState(newState) {
    Object.assign(state, newState);
  }

  function addPlayer(command) {
    const playerID = command.playerID;
    const playerX = "playerX" in command ? command.playerX : Math.floor(Math.random() * state.screen.width);
    const playerY = "playerY" in command ? command.playerY : Math.floor(Math.random() * state.screen.height);

    state.players[playerID] = {
      x: playerX,
      y: playerY
    };

    notifyAll({
      type: "add-player",
      playerID,
      playerX,
      playerY
    });
  }

  function removePlayer(command) {
    const playerID = command.playerID;
    delete state.players[playerID];

    notifyAll({
      type: "remove-player",
      playerID
    });
  }

  function addFruit(command) {
    const fruitID = command ? command.fruitID : Math.floor(Math.random() * 10000000);
    const fruitX = command ? command.fruitX : Math.floor(Math.random() * state.screen.width);
    const fruitY = command ? command.fruitY : Math.floor(Math.random() * state.screen.height);

    state.fruits[fruitID] = {
      x: fruitX,
      y: fruitY
    };

    notifyAll({
      type: "add-fruit",
      fruitID,
      fruitX,
      fruitY
    });
  }

  function removeFruit(command) {
    const fruitID = command.fruitID;
    delete state.fruits[fruitID];

    notifyAll({
      type: "remove-fruit",
      fruitID
    });
  }

  function movePlayer(command) {
    notifyAll(command);

    const acceptedMoves = {
      ArrowUp(player) {
        if (player.y - 1 >= 0) {
          player.y = player.y - 1;
        }
      },
      ArrowRight(player) {
        if (player.x + 1 < state.screen.width) {
          player.x = player.x + 1;
        }
      },
      ArrowDown(player) {
        if (player.y + 1 < state.screen.height) {
          player.y = player.y + 1;
        }
      },
      ArrowLeft(player) {
        if (player.x - 1 >= 0) {
          player.x = player.x - 1;
        }
      }
    };

    const keyPressed = command.keyPressed;
    const playerID = command.playerID;
    const player = state.players[playerID];
    const moveFunction = acceptedMoves[keyPressed];

    if (player && moveFunction) {
      moveFunction(player);
      checkForFruitCollision(playerID);
    }
  }

  function checkForFruitCollision(playerID) {
    const player = state.players[playerID];

    for (const fruitID in state.fruits) {
      const fruit = state.fruits[fruitID];

      if (player.x == fruit.x && player.y == fruit.y) {
        removeFruit({ fruitID });
      }
    }
  }

  return {
    movePlayer,
    removePlayer,
    addPlayer,
    addFruit,
    removeFruit,
    state,
    setState,
    subscribe,
    start
  };
}
