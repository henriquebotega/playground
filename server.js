import express from "express";
import http from "http";
import createGame from "./public/game.js";
import io from "socket.io";

const app = express();
const server = http.createServer(app);
const sockets = io(server);

app.use(express.static("public"));

const game = createGame();
game.start();

game.subscribe(command => {
  sockets.emit(command.type, command);
});

sockets.on("connect", socket => {
  const playerID = socket.id;

  game.addPlayer({ playerID });

  socket.emit("setup", game.state);

  socket.on("disconnect", () => {
    game.removePlayer({ playerID });
  });

  socket.on("move-player", command => {
    command.playerID = playerID;
    command.type = "move-player";

    game.movePlayer(command);
  });
});

server.listen(3000, () => {
  console.log("Server is running...");
});
